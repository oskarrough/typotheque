// Si plusieurs entrées, les séparer par des virgules

nom = fontName;
styles = regular, italic;
catégories = sérif;
auteurs = John Doe;
lien = http://inter.net;
version = 1.0;
licence = Sil Open Font Licence;

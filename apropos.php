<?php include('inc/variables.php') ?>
<?php include('inc/functions.php') ?>
<?php include('inc/head.php') ?>
<?php include('inc/nav.php') ?>

<div id="content" class="apropos">
  <p>
    Ce site est une sélection de caractères typographiques sous <a href="https://en.wikipedia.org/wiki/Free_license" target="_blank">licences libres</a>. Leurs licences vous offrent une totale liberté d'étude, d'usage, de modification et de distribution. Il est actuellement en version béta.
  </p>
  <div class="colophon">
    <h1>Colophon</h1>
    <p>
      Ce site a été mis en place et est administré par le collectif <a href="#" target="_blank">...</a> et composé grâce au caractère typographique libre <a href="https://gitlab.com/Antoine-Gelgon/Ocr-Pbi/" target="_blank">OCR-PBI</a>. Il utilise la librairie javascript <a href="http://opentype.js.org/" target="_blank">opentype.js</a>. Son code source est disponible sur <a href="https://gitlab.com/atelier-bek/typotheque" target="_blank">Gitlab</a> sous licence <a href="https://www.gnu.org/licenses/gpl.html">GNU/GPL</a>. Pour nous contacter ou nous suggérer des fontes, c'est <a href="mailto:">ici</a>.
    </p>
  </div>
  <div class="culturelibre">
    <h1>Culture libre et typographie</h1>
    <?php include('php/cultureLibreTypo.php') ?>
  </div>
  </div>

<?php include('inc/foot.php') ?>

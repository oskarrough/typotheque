<?php
  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('inc/nav.php');
?>

  <div id="content">

    <?php

      foreach ($dirs as $dir){

        $files      = array_diff(scandir($path.'/'.$dir), array('.', '..'));

        $txtFileInfos  = file_get_contents($path.'/'.$dir.'/infos.txt');

        if($txtFileInfos){

          // extraire les données du fichier texte (voir functions.php)
          $styles   = explode( ', ', extract_content($txtFileInfos, 'styles = ', ';'));
          $name     = extract_content($txtFileInfos, 'nom = ', ';');
          $cat      = extract_content($txtFileInfos, 'catégories = ', ';');
          $author   = extract_content($txtFileInfos, 'auteurs = ', ';');
          $link     = extract_content($txtFileInfos, 'lien = ', ';');
          $version  = extract_content($txtFileInfos, 'version = ', ';');

        }

      // Remplir les infos. Si fichier texte, elles viennent
      // en priorité, sinon en jquery avec opentype.js
    ?>

      <div class="fonte <?= $dir ?>" data-type="<?= strtolower($dir) ?>">
        <ul class="infos">
          <li class="name"><h2>Nom:</h2> <?php if($txtFileInfos && $name) echo '<span>'.$name.'</span>' ?></li>
          <li class="author"><h2>Auteur(s):</h2> <?php if($txtFileInfos && $author) echo '<span>'.$author.'</span>' ?></li>
          <li class="styles"><h2>Style(s):</h2> <?php if($txtFileInfos && $styles) echo '<span>'.count($styles).'</span>' ?></li>
          <li class="category"><h2>Categorie(s):</h2> <?php if($txtFileInfos && $cat){ echo $cat; } else { echo '?'; } ?></li>
          <li class="download"><a href="<?php if($txtFileInfos && $link) echo $link ?>" target="_blank">Download</a></li>
          <li class="specimen"><a href="specimen.php?font=<?= $dir ?>">Spécimen</a></li>
        </ul>
        <div class="deroule">
          ↕
        </div>
        <?php
          // echo '<h1>'.$dir.'</h1>';

          foreach($files as $i => $file){

            $fileInfos = pathinfo($file);
            $fontExt = Array('ttf', 'otf');

            if( in_array($fileInfos[extension], $fontExt)){
              $nameSlug = str_replace(' ', '', $fileInfos[filename]);

              echo '
                <style>
                  @font-face {
                    font-family: '.$nameSlug.';
                    src: url('.$path.'/'.$dir.'/'.$file.');
                  }
                </style>
              ';
              echo '<p class="sample '.$i.' '.$fileInfos[filename].'.'.$fileInfos[extension].'" style="font-family: '.$nameSlug.', substitut"><input class="demo" type="text" value="Hello world."></p>';

            }
          }

        ?>
      </div>
      <?php
      }
    ?>

  </div>

<?php include('inc/foot.php') ?>

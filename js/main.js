$(document).ready(function(){

})

$(window).load(function(){

  var path = '.././fonts/';

  var dirs = [];
  var firstFonte = [];

  var nav         = $('#nav');
  var navBtnL     = nav.children('.left').children('li');
  var navBtnR     = nav.children('.right').children('li');
  var navInside   = nav.find('.inside');
  var navRange    = nav.find('.right .fontSize input');
  var navCat      = nav.children('.left').children('.categories');
  var navCatLeft  = navCat.position().left;


  var content     = $('#content');
  var fontes      = content.find('.fonte');
  var fontesDemo  = fontes.children('.sample').children('.demo');
  var deroule     = fontes.children('.deroule')

  var specBtn     = $('#specNav ul').children('li');

  var specimen    = $('#specimen');
  var specPart    = specimen.children('.part');
  
  if (specimen.length > 0){
    navRange.css({'visibility': 'hidden'});
  }
  

  fontes.each(function(){
    var fonte = $(this).attr('class').split(' ')[1];
    dirs.push(fonte);
    var first = $(this).children('.sample').eq(0).attr('class').split(' ')[2];
    firstFonte.push(first);
  })


  navCat.next('.inside').css({marginLeft: navCatLeft});

  for(var i = 0; i < dirs.length; i++){
    var fonte   = path+dirs[i]+'/'+firstFonte[i];
    var dirPath = path+dirs[i]+'/';
    showFontInfosHome(fonte, dirs[i], dirPath);
    getNavInfos(fonte, dirs[i], dirPath);
    //showMetrics(fonte, dirs[i]);
  }

  //  functions nav
  showInside(nav, navBtnL, navInside);
  showInside(nav, navBtnR, navInside);
  changeFontSize(navRange, fontesDemo);
  changeColor(fontesDemo);

  changeInput(fontes);

  // home
  showRegularHome(fontes);
  derouleHome(deroule);
  searchFont(fontes);

  // Spécimens
  showfontInfosSpec(path);
  showSpecPart(specBtn, specimen, specPart);
  showCharMap(path);

})


// afficher les infos fonte sur la home (priorité au fichier texte)
function showFontInfosHome(fonte, dir, dirPath){
  opentype.load(fonte, function(err, font) {
    if (err) {
      console.log('Could not load font: ' + err);
    } else {

      var infos     = $('#content .'+dir+' .infos');

      // Naming table
      var fontNamingTable = font.tables.name;
      var fontName        = fontNamingTable.fontFamily;
      var fontFamily      = fontNamingTable.preferredFamily;
      var fontAuthor      = fontNamingTable.designer;
      var fontAuthorUrl   = fontNamingTable.designerURL;
      //var fontStyle       = fontNamingTable.fontSubfamily;

      //HeaderTable
      var fontHeaderTable = font.tables.head;
      var fontVersion     = fontHeaderTable.version;
      var fontCreated     = fontHeaderTable.created;
      var fontModified    = fontHeaderTable.modified;

      if (fontFamily == null) {
        getInfoHome('name', fontName, infos);
      } else {
        getInfoHome('name', fontFamily, infos);
      }

      getInfoHome('author', fontAuthor, infos);
      //getInfoHome('styles', fontStyle, infos);

      if (fontAuthorUrl) {
        $.each(fontAuthorUrl, function(index, value){
          if (value.indexOf('http') == -1) {
            value = 'http://'+value;
          }
          infos.children('.author').children('span').wrap('<a href="'+value+'" target="_blank"></a>');
        })
      }

    }
  })
}

// Récupérer les infos fonte pour nav
function getNavInfos(fonte, dir, dirPath){
  opentype.load(fonte, function(err, font) {
    if (err) {
      console.log('Could not load font: ' + err);
    } else {

      var fontList =  $('#nav').children('.left').children('.fontListInside');

      // Naming table
      var fontNamingTable = font.tables.name;
      var fontName        = fontNamingTable.fontFamily;
      var fontFamily      = fontNamingTable.preferredFamily;

      if (fontFamily == null) {
        getInfo('name', fontName, fontList);
      } else {
        getInfo('name', fontFamily, fontList);
      }

    }
  })
}


// afficher les infos fonte sur les spécimens (priorité au fichier texte)
function showfontInfosSpec(path){
  var fontInfos       = $('#fontInfos');
  var font            = fontInfos.attr('class');
  var fontListPush    = fontInfos.children('ul').first();
  var fontAuthorPush  = fontInfos.children('.auteurs');
  var fontVersionPush = fontInfos.children('.version');
  var fontModifPush   = fontInfos.children('.modif');
  var fontVariantes   = fontListPush.attr('class').split(' ');

  for(var i = 0; i < fontVariantes.length; i++){
    var fonte = path+font+'/'+fontVariantes[i];

    opentype.load(fonte, function(err, font) {
      if (err) {
        console.log('Could not load font: ' + err);
      } else {

        // Naming table
        var fontNamingTable = font.tables.name;
        var fontStyle       = fontNamingTable.fontSubfamily;

        getInfo(fontVariantes[i], fontStyle, fontListPush);
      }
    })

  }

  var fonte = path+font+'/'+fontVariantes[0];

  opentype.load(fonte, function(err, font) {
    if (err) {
      console.log('Could not load font: ' + err);
    } else {
      // Naming table
      var fontNamingTable = font.tables.name;
      var fontName        = fontNamingTable.fontFamily;
      var fontFamily      = fontNamingTable.preferredFamily;
      var fontAuthor      = fontNamingTable.designer;
      var fontAuthorUrl   = fontNamingTable.designerURL;
      var fontStyle       = fontNamingTable.fontSubfamily;
      var fontVersion     = fontNamingTable.version;

      //HeaderTable
      var fontHeaderTable = font.tables.head;
      var fontCreated     = fontHeaderTable.created;
      var fontModified    = fontHeaderTable.modified;

      if (fontFamily == null) {
        getInfo(fontVariantes[1], fontName, fontInfos.children('h1'));
      } else {
        getInfo(fontVariantes[1], fontFamily, fontInfos.children('h1'));
      }

      getInfo(fontVariantes[1], fontAuthor, fontAuthorPush);
      getInfo(fontVariantes[1], fontVersion, fontVersionPush);
      getDate('', fontModified, fontModifPush);

      if (fontAuthorUrl) {
        $.each(fontAuthorUrl, function(index, value){
          fontInfos.children('.auteurs').children('li').wrap('<a href="'+value+'" target="_blank"></a>');
        })
      }

    }
  })

}

// fonction appelé dans la fonction showFontInfosHomme, pour afficher l'info
function getInfoHome(type, object, infos){
  if(object){
    $.each(object, function(index, value){
      if(infos.children('.'+type).children('span').length > 0 ){
      } else {
        infos.children('.'+type).append('<span>'+value+'</span>');
      }
    })
  } else{
      infos.children('.'+type).append('<li>?</li>');
  }

}

// fonction appelé dans la fonction showFontInfosSpec, pour afficher l'info
function getInfo(type, object, infos){
  if(object){
    $.each(object, function(index, value){
      var valueClass = value.replace(' ', '');
      if (infos.children('li').length > 0) {
      } else {
        infos.append('<li class="'+valueClass+'">'+value+'</li>');
      }
    })
  } else{
      infos.append('<li>?</li>');
  }

}

// spécial pour afficher la date de création
function getDate(type, date, infos){
  var dateToFormat = new Date(date);
  var d = dateToFormat.getDate();
  var m =  dateToFormat.getMonth();
  m += 1;  // JavaScript months are 0-11
  var y = dateToFormat.getFullYear();
  var fullDate = d+'/'+m+'/'+y;

  infos.append('<li>'+fullDate+'</li>');

}


// switch entre les différentes parties du spécimen
function showSpecPart(btn, specimen, part){

  part.hide();
  part.parent().children('.waterfall').show();

  btn.click(function(){

    var theClass = $(this).attr('class').split(' ')[0];

    part.hide();
    specimen.children('.'+theClass).show();

    btn.removeClass('current');
    btn.parent().children('.'+theClass).addClass('current');

    $(window).scrollTop(0);

  })

}


// afficher la table de caractère
function showCharMap(path){

  var fontInfos       = $('#fontInfos');
  var font            = fontInfos.attr('class');
  var fontListPush    = fontInfos.children('ul').first();
  var fontVariantes   = fontListPush.attr('class').split(' ');

  for(var i = 0; i < fontVariantes.length; i++){
    var fonte = path+font+'/'+fontVariantes[i];
    opentype.load(fonte, function(err, font) {
      if (err) {
        console.log('Could not load font: ' + err);
      } else {

        var fontGlyphs = font.glyphs.glyphs;

        // console.log(fontGlyphs[20]);

        var ctx = document.getElementById('canvas').getContext('2d');
        var path = fontGlyphs[53].getPath(0, 250, 250);
        fontGlyphs[53].drawPoints(ctx, 0, 250, 250);
        fontGlyphs[53].drawMetrics(ctx, 0, 250, 250);
        path.fill = '#999999';
        path.stroke = '#111111';
        path.draw(ctx);

        if(fontGlyphs){
          $.each(fontGlyphs, function(index, value){
            // console.log(value);
          // var ctx = document.getElementById('canvas').getContext('2d');
          // var path = value.getPath()
          // path.draw(ctx);
            // var glyphName =
          // console.log(value.name);

          })
        }
      }
    })
  }
}


// modifier un input modifie tout les inputs de la page
function changeInput(fontes){

  document.addEventListener('input', function(e) {
    if(e.target.className == 'demo'){
      fontes.children('p').children('input').val(e.target.value);
    }
  })

}


// priorité à la régular sur la home si existe
function showRegularHome(fontes){

  var fontDemos = fontes.children('p');

  fontDemos.hide();

  fontDemos.each(function(){

    var fontClass = $(this).attr('class').toLowerCase();

    if (fontClass.indexOf('reg') >= 0 && fontClass.indexOf('book') < 0 && fontClass.indexOf('ital') < 0 && fontClass.indexOf('mono') < 0) {
      $(this).show().addClass('showed');
    } else if (fontClass.indexOf('book') >= 0 && fontClass.indexOf('reg') < 0 && fontClass.indexOf('ital') < 0 && fontClass.indexOf('mono') < 0){
      $(this).show().addClass('showed');
    }
  })

  fontes.each(function(){
    if (!$(this).children('p').is(':visible')) {
      $(this).children('p').first().show().addClass('showed');
    }
    if ($(this).children('p').find(':visible').length > 1) {
      $(this).children('.showed').eq(1).hide().removeClass('showed');
    }
  })


}

// afficher les autres styles sur la home au click
function derouleHome(deroule){
  deroule.click(function(){
    if (!$(this).hasClass('on')) {
      $(this).parent().children('p').show();
      $(this).addClass('on');
    } else {
      $(this).parent().children('p').hide();
      $(this).parent().children('.showed').show();
      $(this).removeClass('on');
    }
  })
}

// afficher les catégories sur nav au click
function showInside(nav, btn, inside){
  btn.click(function(){
    $(this).next('.inside').toggle();
  })
}

// changer font-size en fonction du input range de nav
function changeFontSize(range, demo){
  range.val('100');
  document.addEventListener('input', function(e) {
    if(e.target.className == 'rangeValue'){
      demo.css({'font-size': e.target.value+'px'});
    }
  })
}


// changer la couleur des fontes
function changeColor(fontes){
  $(function() { $('.colorpickerFont').wheelColorPicker(); });
  $(function() { $('.colorpickerBg').wheelColorPicker(); });

  $('.changeCol').click(function(){
    var fontColor = $('.colorpickerFont').wheelColorPicker('getValue');
    var bgColor   = $('.colorpickerBg').wheelColorPicker('getValue');
    fontes.css({'color': '#'+fontColor});
    $('.color').css({
      'color': '#'+fontColor,
      'background-color': '#'+bgColor,
    })
    $('body').css({'background-color': '#'+bgColor});
  })
  $('.reset').click(function(){
    fontes.css({'color': 'black'});
    $('body').css({'background-color': 'white'});
    $('.color').css({
      'color': 'black',
      'background-color': 'white',
    })
  })

}

// Outil de recherche
function searchFont(fontes){

  var items = fontes;
  var itemList = [];
  for (var i = 0; i < items.length; i++) {
  	var list = items[i];
  	itemList.push($(list).attr('data-type'));
  }

  $('input.q').keyup(function(key) {
    var input = $(this).val();
  	if (searchList(input)) {
  		var list = '[data-type*="' + input.toLowerCase() + '"]';
  		items.hide();
  		$(list).show();
  	}
  	else {
  		items.show();
  	}
  });

  function searchList(search) {
  	for (var i = 0; i < itemList.length; i++) {
  		var result = itemList[i].match(new RegExp(search, 'ig'));
  		if (result !== null) {
  			return search;
  		}
  	}
  }

}

// afficher les ascendantes, descendantes et hauteur d'x sur home
function showMetrics(fonte, dir){
  opentype.load(fonte, function(err, font) {
    if (err) {
      console.log('Could not load font: ' + err);
    } else {

      var fontMetrics   = font.tables.os2;
      var fontAscender  = fontMetrics.sTypoAscender;
      var fontDescender = fontMetrics.sTypoDescender;
      var fontXheight   = fontMetrics.sxHeight;

      var par           = $('#content .'+dir+' .showed');


      par.append('<div>'+fontXheight+'</div>');

    }
  })
}

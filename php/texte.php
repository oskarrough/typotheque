<p style="font-size:<?= $i*6 ?>px; width: <?= $i*10?>%">
  L'atelier Bek se compose d'une diversité de pratiques, d'échanges et de savoir-faire. C’est pour cela que cet atelier est composé de plasticiens, designers, architectes, développeurs, bricoleurs qui partagent un même espace permettant d’ouvrir le dialogue et la collaboration, mettant en avant la porosité des frontières entre les disciplines de chacun, conférant ainsi au lieu une dynamique. C'est dans ce même souci d'échange et de transmission que l'atelier organise des workshops, conférences et expositions.
</p>
